import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import Header from './components/Header';
import Post from './components/Post';

import './styles/style.scss';

class App extends Component {
  state = {
    posts: [
      {
        id: 1,
        userData: {
          imageUser:
            'https://images.amcnetworks.com/blogs.amctv.com/wp-content/uploads/2010/04/BB-S3-Bob-Odenkirk-325.jpg',
          nameUser: 'Saul Goodman',
          hourPost: 'Há 3 min',
        },
        postText:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent blandit, metus in mollis commodo, elit nunc congue libero, nec dictum tortor tellus at nisl. Mauris diam sapien, pretium non felis id, ornare volutpat libero. Integer sit amet ultrices orci. Nullam vehicula eleifend semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ultricies, augue nec congue faucibus, magna dolor malesuada nibh, a volutpat justo massa viverra ligula. Pellentesque consequat iaculis lacus, eu euismod ipsum mattis at. Nulla facilisi. Fusce rhoncus viverra est ut efficitur.',
      },
      {
        id: 2,
        userData: {
          imageUser: 'https://cdn-images-1.medium.com/max/800/0*XjNQsHH63-c3KNWM.',
          nameUser: 'Chuck McGill',
          hourPost: 'Há 10 min',
        },
        postText:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent blandit, metus in mollis commodo, elit nunc congue libero, nec dictum tortor tellus at nisl. Mauris diam sapien, pretium non felis id, ornare volutpat libero. Integer sit amet ultrices orci. Nullam vehicula eleifend semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ultricies, augue nec congue faucibus, magna dolor malesuada nibh, a volutpat justo massa viverra ligula. Pellentesque consequat iaculis lacus, eu euismod ipsum mattis at. Nulla facilisi. Fusce rhoncus viverra est ut efficitur.',
      },
      {
        id: 3,
        userData: {
          imageUser:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToThTwECmI18BlT-NKsWcmSJtBLI_ruUYvSJFz6Et3b8VTcggPOQ',
          nameUser: 'Mike Ehrmantraut',
          hourPost: 'Há 20 min',
        },
        postText:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent blandit, metus in mollis commodo, elit nunc congue libero, nec dictum tortor tellus at nisl. Mauris diam sapien, pretium non felis id, ornare volutpat libero. Integer sit amet ultrices orci. Nullam vehicula eleifend semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ultricies, augue nec congue faucibus, magna dolor malesuada nibh, a volutpat justo massa viverra ligula. Pellentesque consequat iaculis lacus, eu euismod ipsum mattis at. Nulla facilisi. Fusce rhoncus viverra est ut efficitur.',
      },
    ],
  };

  render() {
    return (
      <Fragment>
        <Header />
        <div className="container">
          {this.state.posts.map(post => <Post key={post.id} data={post} />)}
        </div>
      </Fragment>
    );
  }
}

render(<App />, document.getElementById('app'));
